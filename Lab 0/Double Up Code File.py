"""
Author: Cade Liberty
Date: 7/31/20

Notes:
This program will take a number from the user as the upper bound and
then display the double of each number up to that number.
"""
def double():
    counter = 1
    double = 1
    global total
    total = 0
    while counter <= answer:
        double = counter
        double = double*2
        print("double up " + str(counter) + " = " + str(double))
        counter = counter + 1
        total = total + double

# Main starts here
prompt = "Please enter a number from 1 - 20:"
while (True):
    answer = int(input(prompt))
    if answer < 0:
        print("Error. Please input a number between 1 and 20")
    elif answer > 20:
        print ("Error. Please input a number between 1 and 20")
    else:
        double()
        break
print("Total = " + str(total))
